from django.urls import include, path
from django.contrib.auth import update_session_auth_hash
from django.urls import re_path
from .resources import auth_resource
from rest_framework import permissions
from sql_prodject import views

app_name = 'sql_prodject'

urlpatterns = [
    path('authorisation', auth_resource.CustomAuthToken.as_view()),
    path('add_to_db',views.add_to_db),
    path('request',views.execute_request)
]