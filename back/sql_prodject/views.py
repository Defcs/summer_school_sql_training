import json

from django.shortcuts import render
from .models import Students
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from .serializers import StudentSerializer

# Create your views here.
@api_view(['POST'])
@permission_classes([AllowAny])
def execute_request(request):
    try:
        quary = request.POST['quary']
    except:
        return JsonResponse({'data':'error'})
    queryset = Students.objects.row(quary)
    serializer = StudentSerializer(queryset,many=False)
    return JsonResponse({'data': serializer.data})

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_to_db(request):
    input_file = request.POST['quary']
    data = json.dumps(input_file)
    serializer = StudentSerializer(data,many=True)
    serializer.is_valid()
    serializer.save()
    return JsonResponse({'data': 'Succsess'})