from django.contrib import admin
from .models import Students

# Register your models here.

class students(admin.ModelAdmin):
    list_display = ('id', 'name', 'sename')
    list_filter = ['name']


admin.site.register(Students, students)

