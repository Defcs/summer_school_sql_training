from django.db import models
from django.contrib import admin

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

# Create your models here.

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class Students(models.Model):
    id = models.AutoField(primary_key=True,auto_created=True)
    name = models.CharField(max_length=200,verbose_name = 'Имя')
    sename = models.CharField(max_length=200,verbose_name = 'Фамилия')

    class Meta:
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'
    
    @admin.display(
        boolean=True,
        ordering='id',
    )
    def __str__(self):
        return '{} {} {}'.format(self.id,self.name,self.sename)
