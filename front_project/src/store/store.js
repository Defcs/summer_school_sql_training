// import axios from "axios";

export const store = {
    state:{
        token:[],
    },
    getters:{
        getToken: state =>{
            return state.token
        },
    },
    mutations:{
        setToken(state,token){
            state.token = token
        }
    },
    actions:{
    }
}
