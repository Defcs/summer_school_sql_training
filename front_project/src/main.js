import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.config.productionTip = false

import mainpage from "@/pages/MainPage"
import cabinetpage from "@/pages/CabinetPage"
import authpage from "@/pages/AuthPage"
// import catalogthirdpage from "@/pages/CatalogThirdCatigories"
// import testpage from "@/pages/TestingPage"
// import productpage from "@/pages/ProductPage"

const routes = [
  { path: '/', component: mainpage },
  { path: '/cabinet', component: cabinetpage },
  { path: '/authorisation', component: authpage },
  // { path: '/catalog/:link/:linktwo', component: catalogthirdpage },
  // { path: '/catalog/:link/:linktwo/:product', component: productpage},
  // { path: '/test', component: testpage },
]

const router = new VueRouter({
  routes 
})

import {store} from '@/store/store'

const storeObj = new Vuex.Store(store);

new Vue({
  store:storeObj,
  router,
  render: h => h(App),
}).$mount('#app')

